#!/bin/bash

mkdir /data && cd $_

apt -y install snapd

systemctl disable --now systemd-resolved.service
chattr -i /etc/resolv.conf
rm -f /etc/resolv.conf
echo 'nameserver 8.8.8.8' > /etc/resolv.conf
echo 'nameserver 1.1.1.1' >> /etc/resolv.conf

snap install microk8s --classic
usermod -a -G microk8s $USER
chown -f -R $USER ~/.kube

echo 'alias kubectl="microk8s kubectl"' >> /root/.bashrc

microk8s enable dns ingress cert-manager

microk8s kubectl apply -f webserver-depl-svc.yaml

sleep 3
microk8s kubectl apply -f ingress-routes.yaml
microk8s kubectl get pods -A

sleep 3
microk8s kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.10.1/cert-manager.yaml

sleep 3
microk8s kubectl get pods -n=cert-manager

echo kubectl apply -f letsencrypt-staging.yaml
echo kubectl apply -f letsencrypt-prod.yaml

echo kubectl apply -f ingress-routes-ssl-staging.yaml 

echo kubectl get certificate
echo kubectl describe certificate tls-secret


echo kubectl apply -f ingress-routes-ssl-prod.yaml
